== Generate slides

Clone the repository (and update its submodule) with:

[source,bash]
----
$ git clone git@gitlab.com:ntfc/asciidoctor-slides-raisin.git
$ git submodule update --init --recursive
----

Now you can use the `build.sh` script to generate the HTML5 slides from `index.adoc`:

[source,bash]
----
$ ./build.sh
----

You can view the slides generated at `index.html` in any browser.
