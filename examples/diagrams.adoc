= Asciidoctor with (text) diagrams
Nuno Carvalho <nuno.carvalho@raisin.com

Example of a graphviz (dot) diagram:

//tag::graphviz[]
[graphviz,examples/diagram-graphviz]
----
digraph G {
    main -> parse -> execute;
    main -> init;
    main -> cleanup;
    execute -> make_string;
    execute -> printf
    init -> make_string;
    main -> printf;
    execute -> compare;
}
----
//end::graphviz[]

Example of ditaa diagram:

// tag::ditaa[]
[ditaa,examples/diagram-ditaa]
----
                   +-------------+
                   | Asciidoctor |-------+
                   |   diagram   |       |
                   +-------------+       | PNG out
                       ^                 |
                       | ditaa in        |
                       |                 v
 +--------+   +--------+----+    /---------------\
 |        | --+ Asciidoctor +--> |               |
 |  Text  |   +-------------+    |   Beautiful   |
 |Document|   |   !magic!   |    |    Output     |
 |     {d}|   |             |    |               |
 +---+----+   +-------------+    \---------------/
     :                                   ^
     |          Lots of work             |
     +-----------------------------------+
----
// end::ditaa[]

And an example with plantuml:

// tag::plantuml[]
[plantuml,examples/diagram-plantuml,png]     
----
class BlockProcessor
class DiagramBlock
class DitaaBlock
class PlantUmlBlock

BlockProcessor <|-- DiagramBlock
DiagramBlock <|-- DitaaBlock
DiagramBlock <|-- PlantUmlBlock
----
// end::plantuml[]

// tag::plantuml-sequence[]
[plantuml,examples/diagram-sequence-plantuml,png]
----
actor Customer #gray
entity OBS #blue
entity OpSys #orange
entity Starling as st #red

autonumber

==payin from reference account to transaction account==

Customer -[#gray]> st      : Transfers money from reference acc to Transaction acc.

Customer -[#gray]-> st    : Some point in time, customer's personal bank \ntransfers the money to the Transaction account.
st       -[#red]-> OpSys  : Call PaymentConfirmation webhook
OpSys    -[#orange]-> OBS : Invoke PaymentConfirmation endpoint
OBS     -[#blue]> OBS : Creates the new already booked transaction for \nthis "Topup".
----
// end::plantuml-sequence[]
